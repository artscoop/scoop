��    V      �     |      x     y          �     �     �     �     �     �     �     �     �     �  .   �     '     A     I     Q     X     ^     t     �  	   �     �     �     �     �     �  
   �     �     �  
   	     	     	     %	     *	     1	     6	     G	  
   X	     c	     j	     w	  4   |	     �	     �	     �	     �	     �	  &   �	     �	     �	     
     !
     /
     =
     E
     U
  	   ^
  
   h
     s
     
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
                    ,     ?     N     ]     p     x     �     �     �  !   �  !   �  �  �     s  	   z     �     �     �     �     �  	   �     �  )   �     �       -        =     [     c  
   j     u     {     �     �     �     �  
   �     �       #        9     O     b     k     x     |     �     �     �     �     �  	   �     �     �     �  3   �     2     D     J     P     U  7   ^     �  *   �     �     �     �     �  	   �     �     �          "  	   .     8     E     M     T     \     e  
   n  	   y     �  	   �     �     �     �  	   �     �     �  	   �     �  
   �                       $   @     "   R           2       G          T   ?             D   I   5   7      6   O   M       
      -       )   8             #               9       1           .            %       =   /      >   <   H       +       	   $      A           ,   &                  @              C          K   *                                    L          !   N                 E       4             (          J   '      3   P       U      S   ;       :          V   F      B       0   Q        1 day 1 week 2 hours 3 days 30 days All messages Always on top Answers Author Can be set at thread creation Choice Description Does the label define the status of the thread Enter one answer per line Expired Expires Expiry Forum Forum label: [{name}] Groups allowed HTML description Has votes Hide selected threads Label Labels Last speaker Lock selected threads Main label Message count Messages Moderators Name No description Open Parent Poll Posting disabled Reading disabled Short name Status Status label Text The selected threads have been successfully updated. Thread Title Topic Type Unnamed Update information of selected threads User Vote: {vote} in poll "{poll}" Votes labelPrimary labelVisible message messageDeleted messages mute list mute lists participant participantIs active participants poll pollClosed pollPublished polls sanction sanctionRevoked sanctions thread threadDeleted threadEdited threadLocked threadUnread threadUnread time threadUpdated threadVisible threadWas started threads translation translations vote votes {count} threads have been hidden. {count} threads have been locked. Project-Id-Version: 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-07-01 04:39+0200
PO-Revision-Date: 2016-07-01 04:45+0009
Last-Translator: b'Anonymous User <steve.kossouho@gmail.com>'
Language-Team: français <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n!=1);
X-Translated-Using: django-rosetta 0.7.11
 1 jour 1 semaine 2 heures 3 jours 30 jours Tous les messages Toujours en tête Réponses Auteur Peut-être défini à la création du fil Choix Description L'étiquette définit-elle le statut du fil ? Entrez une réponse par ligne Expiré Expire Expiration Forum Étiquette de forum : [{name}] Groupes autorisés Description HTML A des votes Masquer les fils sélectionnés Étiquette Étiquettes Dernier participant Verrouiller les fils sélectionnés Étiquette principale Nombre de messages Messages Modérateurs Nom Aucune description Ouvert Parent Sondage Réponses désactivées Lecture désactivée Nom court Statut Étiquette de statut Texte Les fils sélectionnés ont bien été mis à jour. Fil de discussion Titre Sujet Type Sans nom Mettre à jour les informations des fils sélectionnés Utilisateur Vote : {vote} dans le sondage « {poll} » Votes Primaire Visible message Supprimé messages liste d'ignorés listes d'ignorés participant Est actif participants sondage Fermé Publié sondages sanction Révoquée sanctions fil de discussion Supprimé Modifié Verrouillé Non lu Non lu le Mis à jour Visible Démarré fils de discussion traduction traductions a des votes votes {count} fils ont été masqués. {count} fils ont été verrouillés. 